from django.db import models
from django.contrib.auth.models import User
from PIL import Image

# Create your models here.
class ProfileUser(models.Model):
  # if user delete, delete profile as well. but if profile delete, user still exists
  user = models.OneToOneField(User,on_delete=models.CASCADE)
  image = models.ImageField(default='default.jpg',upload_to='profile_pics')


  def __str__(self):
      return f'{self.user.username} Profile'


  # Re-Size the Image
  # Overwrite the save method, this method is ran after the model is saved
  # We are creating our own to add some functionalities
  # super().save()=>ran save of the parent class,this would be ran anyway wihtout using this func
  def save(self):
    super().save() 
    
    # Open the image of the current instance
    img = Image.open(self.image.path)
    if img.height > 300 or img.width >300:
      output_size = (300,300)
      img.thumbnail(output_size)
      img.save(self.image.path)


      




# OneToOne relation with the User model
# get user from User model then => user.profileuser
# user.profile.image
# user.profile.image.width
# user.profile.image.hieght
# user.profile.image.size
# user.profile/image.url
  
