# signal that is fired after an object has been save
# In this case create and save a ProfileUser when the user is created and saved
from django.db.models.signals import post_save 
from django.contrib.auth.models import User
from django.dispatch import receiver

# A profile will be created from this post_save signal
# We need a user profile to be created each time a new user is registered
from .models import ProfileUser 


@receiver(post_save,sender=User)  # run when a user has been created 
def create_profile(sender,instance,created,**kwargs):
  if created:
    ProfileUser.objects.create(user=instance)   # user in the ProfileUser


@receiver(post_save,sender=User)  # run when a user has been save 
def save_profile(sender,instance,**kwargs):
  instance.profileuser.save()
  

