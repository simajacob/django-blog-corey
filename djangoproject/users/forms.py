from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import ProfileUser

class UserRegisterForm(UserCreationForm):
  email = forms.EmailField()  # default required=True
  class Meta:   
    model = User
    fields = ['username','email','password1','password2']


# To update user name and email
class UserUpdateForm(forms.ModelForm):
  email = forms.EmailField()
  class Meta:
    model = User
    fields = ['username','email']



# To update the image on Profile user
class ProfileUpdateForm(forms.ModelForm):
  class Meta:
    model = ProfileUser
    fields = ['image']
