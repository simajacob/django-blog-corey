from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse

class PostModel(models.Model):
  author = models.ForeignKey(User,on_delete=models.CASCADE)  
  title = models.CharField(max_length=100)
  content = models.TextField()
  date_posted = models.DateTimeField(default=timezone.now) 
  # auto_now=True=>date change everytime post updated, good for last modified field
  # auto_now_add=True => you cannot change the value of the date posted
  # user.postmodel_set.all()
  # user.postmodel_set.create(title='blog helas',content='lela')  # author will be this user
  

  def __str__(self):
      return self.title
  
  # Done for class base createview to redirect to the detail view of the newly
  # created post
  def get_absolute_url(self):
      return reverse("blog:post-detail", kwargs={"pk": self.pk})
  
  

  







# python manage.py sqlmigrate blog 0001
