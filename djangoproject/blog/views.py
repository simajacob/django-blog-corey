from django.shortcuts import render,get_object_or_404
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin,UserPassesTestMixin
from django.http import HttpResponse
from .models import PostModel
from django.views.generic import (
  ListView,
  DetailView,
  CreateView,
  UpdateView,
  DeleteView)


# Home View
def home(request):
  context = {
    'posts': PostModel.objects.all(),
    'current':True
  }
  return render(request,'blog/home.html',context)



class PostListView(ListView):
  model = PostModel
  # search for <app>/<model>_<viewtype>.html but can be replace with a specific template
  template_name = 'blog/home.html'  

  # object_list si the default List that is set from ListView, in this case
  # it can be changed to post since the template was already built with posts List
  context_object_name = 'posts'

  # add ordering on the query_set of ListView, the query_set is ran automatically 
  # even though it is not added in code block of PostListView
  ordering = ['-date_posted']  # get the lastest post

  # Add pagination :
  #  https://docs.djangoproject.com/en/3.0/topics/pagination/#using-paginator-in-view
  # /objects/?page=3
  # /objects/?page=last
  # path('objects/page<int:page>/', PaginatedView.as_view()), 
  paginate_by = 5


class UserPostListView(ListView):
  model = PostModel
  template_name = 'blog/user_posts.html'
  context_object_name = 'posts'
  paginate_by = 5


  def get_queryset(self):
    user = get_object_or_404(User,username=self.kwargs.get('username'))
    return PostModel.objects.filter(author=user).order_by('-date_posted')





class PostDetailView(DetailView):
  # search for <app>/<model>_<viewtype>.html = blog/postmodel_detail.html
  # object is the single data on the template
  model = PostModel




# The CreateView page displayed to a GET request uses a template_name_suffix of '_form'. For example, changing this attribute to '_create_form' for a view creating objects for the example Author model would cause the default template_name to be 'myapp/author_create_form.html'.
class PostCreateView(LoginRequiredMixin,CreateView):
  model = PostModel
  fields = ['title','content']

  # In case you would like to redirect to home page without using the detailview as default
  # success_url = '/'   

  # overwrite the form valid method
  # since the author of the block is unknown we need to add the author
  def form_valid(self, form):
    form.instance.author = self.request.user
    return super().form_valid(form)   # then validate the form

 



# The UpdateView page displayed to a GET request uses a template_name_suffix of '_form'. For example, changing this attribute to '_update_form' for a view updating objects for the example Author model would cause the default template_name to be 'myapp/author_update_form.html'.
class PostUpdateView(LoginRequiredMixin,UserPassesTestMixin,UpdateView):
  model = PostModel
  fields = ['title','content']

  def form_valid(self,form):
    form.instance.author =  self.request.user
    return super().form_valid(form)
  
  # check if the current user is the author of the post before updating
  # must be called test_func() from UserPassesTestMixin
  def test_func(self):
    # get the current post
    post = self.get_object()
    if self.request.user == post.author:
      return True
    return False





# The DeleteView page displayed to a GET request uses a template_name_suffix of '_confirm_delete'. For example, changing this attribute to '_check_delete' for a view deleting objects for the example Author model would cause the default template_name to be 'myapp/author_check_delete.html'.
class PostDeleteView(LoginRequiredMixin,UserPassesTestMixin,DeleteView):
  model = PostModel
  success_url = reverse_lazy('blog:home')
  

  def test_func(self):
    # get the current post
    post = self.get_object()
    if self.request.user == post.author:
      return True
    return False










# About View
def about(request):
  return render(request,'blog/about.html',{'title':'About','current':True})








